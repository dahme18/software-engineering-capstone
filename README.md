# Vacation Scheduler Mobile App

Vacation Scheduler is an Android mobile app created in Android Studio for Android 8 or higher. It allows users to create, update, delete, and share Vacations and excursions. It also has a reporting table for vacation and searching functionality.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Android Studio
Android 8 or higher
```

### Installing

A step by step series of examples that tell you how to get a development env running

1. Download and install Android Studio
2. Clone the repository
3. Open the project in Android Studio
4. Build and run the project

## Usage

To use the app, user needs to login with a username and password. After logging in, user can create, update, delete, and share Vacations and excursions. It also has a reporting table for vacation and searching functionality.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors

* **Dawood Ahmed** - *Software Engineering Student at WGU* -  (https://gitlab.com/dahme18/software-engineering-capstone)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
